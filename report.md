| NUmber | Stages                 | Start date  |     End Date      | Commit                                                                           |
| ------:| ---------------------- |:-----------:|:-----------------:| -------------------------------------------------------------------------------- |
|      1 | Task Clarification     | 06.04.2023  |    08.04.2023     | Reviewed course project introduction                                             |
|      2 | Analysis               | 09.04.2023  |    12.04.2023     | Familiarized with the requirements                                               |
|      3 | Use Cases              | 13.04.2023  |    17.04.2023     | What the commands are and how they work are discussed                            |
|      4 | Search for Solutions   | 18.04.2023  |    25.04.2023     | Researched , how to implement controller, service, dao, entity, exception layers |
|      5 | Software Development   | 26.04.2023  |    25.05.2023     | Almost finished project.Now Im optimizing code                                   |
|      6 | Development Completion | 30.05.2023  |    04.06.2023     | Optimized code, exception handled, 1 version of project is ready                 |
|      7 | Presentation           | 07.06.2023  |    08.06.2023     | Preparing presentation, everything is ready presenting                           |