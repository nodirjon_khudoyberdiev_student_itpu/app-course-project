# Printed products warehouse


This Java project designed to create Java console application that allows users to search for products in a warehouse inventory using the required combination of parameters (e.g., price, quantity, etc.). The system reads the inventory data from a pre-formatted text file and allows users to search using a command-line interface. This README file aims to guide you through the project setup, dependencies, and how to run it successfully.

***

## Table of Contents
- Prerequisites
- Installation
- Running the Project
- Project commands
- Example Usage
- How to see Errors


*** 

## Prerequisites
Before running this project, make sure you have the following prerequisites installed on your system:

- Java Development Kit (JDK) minimum version required.
- Other dependencies or software required, if any.

***

## Installation

To install and set up the project, follow these steps:

1. Clone the project repository to your local machine using the following command:
   git clone https://gitlab.com/nodirjon_khudoyberdiev_student_itpu/app-course-project

2. Navigate to the project directory:
   cd [project directory]

3. Ensure that required dependencies are installed and configured


***

## Running the Project

Once the Java source code is compiled, you can run the Java program using run button (⏵)  or using the "java" command followed by the name of the main class (the class that contains the "main" method) For example:
java Main.java

***

- [ ] 

## Project commands

1. Search           
2. View all products
3. Exit

If you choose you will see :

Please select one of them to use search options

Clothing                                       
Footwear                                       
Back

after choosing one of them you will see :

Search:

       1. By category    
       2. By gender      
       3. By brand       
       4. By price       
       0. Back           




If you go: 
View all products: 

       1.By category    
       2.By gender      
       3.By brand       
       4.By price       
       0.Back   



Sort ⩔     :     
1.By price        
2.By brand(A-Z)   
0.Back

If you go Back:
application will stop

## Note
- The application supports sorting and searching for the selected product category only.
- The search command will provide a list of products that match the specified search term or keyword.
- Ensure that you follow the instructions accurately to get the desired results.

***

## How to see Errors

To see errors when writing a wrong command, Any errors and exceptions are handled in very correct way
#### Developer : 
Nodirjon_Khudoyberdiev@student.itpu.uz
