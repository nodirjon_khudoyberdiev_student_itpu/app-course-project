package edu.itpu.exception;

public class ProductNotFoundException extends RuntimeException{
    private String message;
    private int status;

    public ProductNotFoundException(String message, int status) {
        this.message = message;
        this.status = status;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "MyCustomException{" +
                "message='" + message + '\'' +
                ", status=" + status +
                '}';
    }
}
