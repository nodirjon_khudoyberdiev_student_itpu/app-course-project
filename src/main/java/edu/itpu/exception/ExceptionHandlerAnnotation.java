package edu.itpu.exception;

import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ExceptionHandlerAnnotation {
    Class<? extends Throwable>[] value() default {};
}
