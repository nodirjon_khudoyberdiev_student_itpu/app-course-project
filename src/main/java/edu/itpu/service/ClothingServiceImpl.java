package edu.itpu.service;

import edu.itpu.dao.ClothingDAO;
import edu.itpu.dao.ClothingDAOImpl;
import edu.itpu.dao.ClothingParser;
import edu.itpu.entity.Clothing;

import java.util.*;
import java.util.stream.Collectors;

public class ClothingServiceImpl implements ClothingService {
    private final ClothingDAO clothingDAO;
    private final ClothingParser clothingParser;

    public ClothingServiceImpl(ClothingDAO clothingDAO, ClothingParser clothingParser) {
        this.clothingDAO = clothingDAO;
        this.clothingParser = clothingParser;
    }

    public List<Clothing> getAllClothingList() {
        return clothingParser.parseStringArrToClothing(clothingDAO.getDataFromCsvFile());
    }


    @Override
    public Set<?> getAllCategoryNames() {
//        Set<String> categorySet = new HashSet<>();
//        for (Clothing dataAsAClosing : getDataAsAClosings()) {
//            categorySet.add(dataAsAClosing.getCategory());
//        }
//        return categorySet;
        Set<String> categories = getAllClothingList()
                .stream()
                .map(Clothing::getCategory)
                .collect(Collectors.toSet());
        if (categories.isEmpty()) {
            System.err.println("Any categories not found for now");
            return new HashSet<>();
        }
        return categories;

    }

    @Override
    public List<?> getAllByCategory(String categoryName) {
//        List<Clothing> dataAsAClosings = getDataAsAClosings();
//        List<Clothing> clothingList = new ArrayList<>();
//        for (Clothing dataAsAClosing : dataAsAClosings) {
//            if (dataAsAClosing.getCategory().equalsIgnoreCase(categoryName)){
//                clothingList.add(dataAsAClosing);
//            }
//        }
        List<Clothing> list = getAllClothingList()
                .stream()
                .filter(clothing -> clothing.getCategory().equalsIgnoreCase(categoryName))
                .collect(Collectors.toList());
        if (list.isEmpty()) {
            System.err.println("Nothing found by your input category.Please try again another parameter!!!");
            return new ArrayList<>();
        }
        return list;
    }

    @Override
    public Set<?> getAllGenderNames() {
        Set<String> genders = getAllClothingList()
                .stream()
                .map(Clothing::getGender)
                .collect(Collectors.toSet());
        if (genders.isEmpty()) {
            System.err.println("Any gender not found.Please try agaain!!");
            return new HashSet<>();
        }
        return genders;
    }

    @Override
    public List<?> getAllByGender(String genderName) {
        List<Clothing> clothingList = getAllClothingList()
                .stream()
                .filter(clothing -> clothing.getGender()
                        .equalsIgnoreCase(genderName))
                .collect(Collectors.toList());
        if (clothingList.isEmpty()) {
            System.err.println("Nothing found by your gender name.Please try again!!!");
            return new ArrayList<>();
        }
        return clothingList;
    }

    @Override
    public Set<?> getAllBrandNames() {
        Set<String> brands = getAllClothingList()
                .stream()
                .map(Clothing::getBrand)
                .collect(Collectors.toSet());
        if (brands.isEmpty()) {
            System.err.println("Any brands not found");
            return new HashSet<>();
        }
        return brands;
    }

    @Override
    public List<?> getAllByBrand(String brandName) {
        List<Clothing> list = getAllClothingList()
                .stream()
                .filter(clothing -> clothing.getBrand().equalsIgnoreCase(brandName))
                .collect(Collectors.toList());
        if (list.isEmpty()) {
            System.err.println("Nothing found by your brand name.Please try again!!!");
            return new ArrayList<>();
        }
        return list;
    }

    @Override
    public List<?> getALlByFromAndToPrice(String fromPrice, String toPrice) {
//        List<Clothing> dataAsAClosings = getDataAsAClosings();
//        List<Clothing> clothingList = new ArrayList<>();
//        for (Clothing dataAsAClosing : dataAsAClosings) {
//            if (dataAsAClosing.getPrice()>= fromPrice && dataAsAClosing.getPrice()<=toPrice){
//                clothingList.add(dataAsAClosing);
//            }
//        }
//        return clothingList;

        try {
            double fromMinPrice = Double.parseDouble(fromPrice);
            double toMaxPrice = Double.parseDouble(toPrice);
            List<Clothing> clothingList = getAllClothingList()
                    .stream()
                    .filter(clothing -> clothing.getPrice() >= fromMinPrice && clothing.getPrice() <= toMaxPrice)
                    .collect(Collectors.toList());
            if (clothingList.isEmpty()) {
                System.err.println("Nothing found by your price parameters. Please try again another!!!");
                return new ArrayList<>();
            }
            return clothingList;

        } catch (Exception e) {
            System.err.println("Invalid price parameter entered.Please try again!!!");
            return new ArrayList<>();
        }
    }

    @Override
    public List<?> getAllAsSortedFromMinToMax() {
        List<Clothing> sorted = getAllClothingList()
                .stream()
                .sorted(Comparator.comparingDouble(Clothing::getPrice))
                .collect(Collectors.toList());
        if (sorted.isEmpty()) {
            System.err.println("Not enough data found to sort");
            return new ArrayList<>();
        }
        return sorted;
    }


    @Override
    public List<?> getAllAsSortedFromMaxToMin() {
        List<Clothing> sorted = getAllClothingList()
                .stream()
                .sorted(Comparator.comparingDouble(Clothing::getPrice)
                        .reversed())
                .collect(Collectors.toList());
        if (sorted.isEmpty()) {
            System.err.println("Not enough data found to sort");
            return new ArrayList<>();
        }
        return sorted;
    }

    @Override
    public List<?> getAllAsSortedByBrandFromAToZ() {
        List<Clothing> sorted = getAllClothingList()
                .stream()
                .sorted((o1, o2) -> o1.getBrand().compareToIgnoreCase(o2.getBrand()))
                .collect(Collectors.toList());
        if (sorted.isEmpty()) {
            System.err.println("Not enough data found to sort");
            return new ArrayList<>();
        }
        return sorted;
    }

    @Override
    public List<?> getAllAsSortedByBrandFromZToA() {
        List<Clothing> sorted = getAllClothingList()
                .stream()
                .sorted((o1, o2) -> o1.getBrand().compareToIgnoreCase(o2.getBrand()) * -1)
                .collect(Collectors.toList());
        if (sorted.isEmpty()) {
            System.err.println("Not enough data found to sort");
            return new ArrayList<>();
        }
        return sorted;
    }

    @Override
    public Clothing add(Clothing clothing) {
        return null;
    }

    @Override
    public Set<String> addCategory(String category) {
        return null;
    }

    @Override
    public String delete(Integer id) {
        return null;
    }
}
