package edu.itpu.service;

import edu.itpu.entity.User;

public interface UserService {
    String login(User user);
}
