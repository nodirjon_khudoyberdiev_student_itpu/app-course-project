package edu.itpu.service;

import edu.itpu.entity.User;

public class UserServiceImpl implements UserService {

    public static final String ADMIN_USERNAME  = "admin";
    public static final String ADMIN_PASSWORD  = "admin";
    @Override
    public String login(User user) {
        if (ADMIN_USERNAME.equals(user.getUsername()) && ADMIN_PASSWORD.equals(user.getPassword())){
            return "200";
        }
        return null;
    }
}
