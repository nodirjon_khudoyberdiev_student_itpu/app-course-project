package edu.itpu.service;

import edu.itpu.entity.Footwear;

import java.util.List;
import java.util.Set;

public interface FootwearService {
    Set<?> getAllCategoryNames();

    List<?> getALLByCategory(String categoryName);

    Set<?> getAllGenderNames();

    List<?> getAllByGender(String genderName);

    Set<?> getAllBrandNames();

    List<?> getAllByBrand(String brandName);

    List<?> getAllByFromAndToPrice(String fromPrice, String toPrice);

    List<?> getAllAsSortedFromMinToMax();

    List<?> getAllAsSortedFromMaxToMin();

    List<?> getAllAsSortedByBrandFromAToZ();

    List<?> getAllAsSortedByBrandFromZToA();

    Footwear add(Footwear footwear);

    Set<String> addCategory(String category);

    String delete(Integer id);
}
