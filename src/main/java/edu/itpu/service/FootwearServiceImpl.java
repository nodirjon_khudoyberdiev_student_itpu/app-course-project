package edu.itpu.service;

import edu.itpu.dao.FootwearDAO;
import edu.itpu.dao.FootwearParser;
import edu.itpu.entity.Footwear;

import java.util.*;
import java.util.stream.Collectors;

public class FootwearServiceImpl implements FootwearService {
    private final FootwearDAO footwearDAO;
    private final FootwearParser footwearParser;

    public FootwearServiceImpl(FootwearDAO footwearDAO, FootwearParser footwearParser) {
        this.footwearDAO = footwearDAO;
        this.footwearParser = footwearParser;
    }

    public List<Footwear> getAllFootwearList() {
        return footwearParser.parseStringArrToFootwear(footwearDAO.getDataFromCsvFile());
    }

    @Override
    public Set<?> getAllCategoryNames() {
        Set<String> categories = getAllFootwearList().stream().map(Footwear::getCategory).collect(Collectors.toSet());
        if (categories.isEmpty()) {
            System.err.println("Not any categories found for now");
            return new HashSet<>();
        }
        return categories;
    }

    @Override
    public List<?> getALLByCategory(String categoryName) {
        List<Footwear> list = getAllFootwearList().stream()
                .filter(footwear -> footwear.getCategory().equalsIgnoreCase(categoryName))
                .collect(Collectors.toList());
        if (list.isEmpty()) {
            System.err.println("Nothing found by your category name.Please try again!!!");
            return new ArrayList<>();
        }
        return list;
    }

    @Override
    public Set<?> getAllGenderNames() {
        Set<String> genders = getAllFootwearList()
                .stream()
                .map(Footwear::getGender)
                .collect(Collectors.toSet());
        if (genders.isEmpty()) {
            System.err.println("Not any genders found");
            return new HashSet<>();
        }
        return genders;

    }

    @Override
    public List<?> getAllByGender(String genderName) {
        List<Footwear> list = getAllFootwearList()
                .stream()
                .filter(footwear -> footwear.getGender().equalsIgnoreCase(genderName))
                .collect(Collectors.toList());
        if (list.isEmpty()) {
            System.err.println("Nothing found by your gender name.Please try again!!!");
            return new ArrayList<>();
        }
        return list;
    }

    @Override
    public Set<?> getAllBrandNames() {
        Set<String> brands = getAllFootwearList()
                .stream()
                .map(Footwear::getBrand)
                .collect(Collectors.toSet());
        if (brands.isEmpty()) {
            System.err.println("Not any brand found");
            return new HashSet<>();
        }
        return brands;
    }

    @Override
    public List<?> getAllByBrand(String brandName) {
        List<Footwear> list = getAllFootwearList()
                .stream()
                .filter(footwear -> footwear.getBrand().equalsIgnoreCase(brandName))
                .collect(Collectors.toList());
        if (list.isEmpty()) {
            System.err.println("Nothing found by your brand name.Please try again!!!");
            return new ArrayList<>();
        }
        return list;
    }

    @Override
    public List<?> getAllByFromAndToPrice(String fromPrice, String toPrice) {
        try {
            double fromMinPrice = Double.parseDouble(fromPrice);
            double toMaxPrice = Double.parseDouble(toPrice);
            List<Footwear> footwears = getAllFootwearList()
                    .stream()
                    .filter(footwear -> footwear.getPrice() >= fromMinPrice && footwear.getPrice() <= toMaxPrice)
                    .collect(Collectors.toList());
            if (footwears.isEmpty()) {
                System.err.println("Nothing found by your input parameter for now.Please try again another price parameter");
                return new ArrayList<>();
            }
            return footwears;
        } catch (Exception e) {
            System.err.println("Invalid price value entered.Please try again!!!");
            return new ArrayList<>();
        }
    }

    @Override
    public List<?> getAllAsSortedFromMinToMax() {
        List<Footwear> sorted = getAllFootwearList()
                .stream()
                .sorted(Comparator.comparingDouble(Footwear::getPrice))
                .collect(Collectors.toList());
        if (sorted.isEmpty()) {
            System.err.println("Not any products found to sort");
            return new ArrayList<>();
        }
        return sorted;
    }

    @Override
    public List<?> getAllAsSortedFromMaxToMin() {
        List<Footwear> sorted = getAllFootwearList()
                .stream()
                .sorted(Comparator.comparingDouble(Footwear::getPrice)
                        .reversed())
                .collect(Collectors.toList());
        if (sorted.isEmpty()) {
            System.err.println("Not any products found to sort");
            return new ArrayList<>();
        }
        return sorted;
    }

    @Override
    public List<?> getAllAsSortedByBrandFromAToZ() {
        List<Footwear> sorted = getAllFootwearList()
                .stream()
                .sorted((o1, o2) -> o1.getBrand().compareToIgnoreCase(o2.getBrand()))
                .collect(Collectors.toList());
        if (sorted.isEmpty()) {
            System.err.println("Not any products found to sort");
            return new ArrayList<>();
        }
        return sorted;
    }

    @Override
    public List<?> getAllAsSortedByBrandFromZToA() {
        List<Footwear> sorted = getAllFootwearList()
                .stream()
                .sorted((o1, o2) -> o1.getBrand().compareToIgnoreCase(o2.getBrand()) * -1)
                .collect(Collectors.toList());
        if (sorted.isEmpty()) {
            System.err.println("Not any products found to sort");
            return new ArrayList<>();
        }
        return sorted;
    }

    @Override
    public Footwear add(Footwear footwear) {
        return null;
    }

    @Override
    public Set<String> addCategory(String category) {
        return null;
    }

    @Override
    public String delete(Integer id) {
        return null;
    }
}
