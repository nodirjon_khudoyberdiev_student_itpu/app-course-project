package edu.itpu.dao;

import java.util.List;

public interface FootwearDAO {
    List<String[]> getDataFromCsvFile();

}
