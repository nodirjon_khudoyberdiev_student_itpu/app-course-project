package edu.itpu.dao;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ClothingDAOImpl implements ClothingDAO {
    final String PATH = "src/main/resources/clothingData.csv";

    @Override
    public List<String[]> getDataFromCsvFile() {
        List<String[]> rows = new ArrayList<>();
        FileReader fileReader;
        try {
            fileReader = new FileReader(PATH);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                String[] product = line.split(",");
                rows.add(product);
            }
            return rows;
        } catch (IOException e) {
            System.err.println("File not found or error occurring this part" + e.getMessage() + e.getCause());
        }
        return new ArrayList<>();
    }

}
