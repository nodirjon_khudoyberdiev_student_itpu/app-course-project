package edu.itpu.dao;

import edu.itpu.entity.Footwear;

import java.util.ArrayList;
import java.util.List;

public class FootwearParser {
    public List<Footwear> parseStringArrToFootwear(List<String[]> data) {
        List<Footwear> footwears = new ArrayList<>();
        for (int i = 1; i < data.size(); i++) {
            String[] obj = data.get(i);
            footwears.add(new Footwear(
                    Integer.parseInt(obj[0]),
                    obj[1],
                    obj[2],
                    obj[3],
                    obj[4],
                    obj[5],
                    Integer.parseInt(obj[6]),
                    obj[7],
                    Integer.parseInt(obj[8]),
                    Double.parseDouble(obj[9]),
                    obj[10]
            ));
        }
        return footwears;
    }
}
