package edu.itpu.dao;

import java.util.List;

public interface ClothingDAO {

    List<String[]> getDataFromCsvFile();
}
