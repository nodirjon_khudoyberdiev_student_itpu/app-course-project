package edu.itpu.dao;

import edu.itpu.entity.Clothing;

import java.util.ArrayList;
import java.util.List;

public class ClothingParser {

    public List<Clothing> parseStringArrToClothing(List<String[]> data) {
        List<Clothing> clothingList = new ArrayList<>();
        for (int i = 1; i < data.size(); i++) {
            String[] obj = data.get(i);
            clothingList.add(new Clothing(
                    Integer.parseInt(obj[0]),
                    obj[1],
                    obj[2],
                    obj[3],
                    obj[4],
                    obj[5],
                    Integer.parseInt(obj[6]),
                    obj[7],
                    Integer.parseInt(obj[8]),
                    Double.parseDouble(obj[9]),
                    obj[10]
            ));
        }
        return clothingList;
    }
}
