package edu.itpu.view;

import edu.itpu.controller.*;
import edu.itpu.dao.*;
import edu.itpu.entity.Clothing;
import edu.itpu.entity.Footwear;
import edu.itpu.entity.User;
import edu.itpu.service.*;
import net.steppschuh.markdowngenerator.table.Table;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class View {
    //text colors to display colorful
    public static final String CYAN_BOLD_BRIGHT = "\033[1;96m";
    public static final String WHITE_BOLD_BRIGHT = "\033[1;97m";
    public static final String BLUE_BOLD_BRIGHT = "\033[1;94m";
    public static final String YELLOW_BACKGROUND = "\033[43m";
    public static final String RESET = "\033[0m";

    public static void main() {
        ClothingDAO clothingDAO = new ClothingDAOImpl();
        ClothingParser clothingParser = new ClothingParser();
        ClothingService clothingService = new ClothingServiceImpl(clothingDAO, clothingParser);
        ClothingController clothingController = new ClothingControllerImpl(clothingService);
        UserService userService = new UserServiceImpl();
        UserController userController = new UserControllerImpl(userService);

        FootwearDAO footwearDAO = new FootwearDAOImpl();
        FootwearParser footwearParser = new FootwearParser();
        FootwearService footwearService = new FootwearServiceImpl(footwearDAO, footwearParser);
        FootwearController footwearController = new FootwearControllerImpl(footwearService);

        System.out.println(CYAN_BOLD_BRIGHT + """
                | █  █ ███ █   █   ████    ███ █████ ████ █  █  █
                | █  █ █   █   █   █  █     █    █   █  █ █  █  █
                | ████ ███ █   █   █  █     █    █   ████ █  █  █
                | █  █ █   █   █   █  █     █    █   █    █  █   
                | █  █ ███ ███ ███ ████    ███   █   █    ████  █  
                """ + RESET);

        System.out.println(CYAN_BOLD_BRIGHT + """
                Project name: Course Project. Clothes and footwear warehouse
                Developer: Nodirjon Khudoyberdiev, nodirjon_khudoyberdiev@student.itpu.uz
                Creation date: April 6th, 2023
                Version: v1
                """ + RESET);
        System.out.println(CYAN_BOLD_BRIGHT + """
                Project name: Course Project. Clothes and footwear warehouse
                Developer: Nodirjon Khudoyberdiev, nodirjon_khudoyberdiev@student.itpu.uz
                Creation date: Jan 1, 2024
                Version: v2
                """ + RESET);

        boolean bigWhile = true;
        boolean isAdmin = false;
        while (bigWhile) {
            Scanner scanner = new Scanner(System.in);
            System.out.println(BLUE_BOLD_BRIGHT + """
                    ----------------------------------------------------------------- 
                    | Choose the role or exit:                                      |
                    | 1.Admin                                                       |
                    | 2.User                                                        |       
                    | 3.Exit                                                        |
                    -----------------------------------------------------------------
                    """);
            String inputCommandRole = scanner.nextLine();

            if (inputCommandRole.equalsIgnoreCase("admin")) {
                System.out.println("Please enter your username");
                String username = scanner.nextLine();
                System.out.println("Please enter your password");
                String password = scanner.nextLine();

                User user = new User();
                user.setUsername(username);
                user.setPassword(password);
                user.setRole("ADMIN");
                String loginResponse = userController.login(user);
                if (loginResponse.equals("200")) {
                    System.out.println("Successfully login");
                    isAdmin = true;


                } else {
                    System.out.println("Invalid creditionals");
                    break;
                }


            }
            if (inputCommandRole.equalsIgnoreCase("user")) {
                System.out.println("Hello user!");

            } else if (inputCommandRole.equalsIgnoreCase("exit")) {
                System.out.println("Stop command entered.\nThank you for your attention!");
                bigWhile = false;
            }

            System.out.println("1.Search \n2.View all product \n3.Exit");
            System.out.println(isAdmin==true ?
                            "4.Add product \n5.Add category \n6.Delete product \n"
                            :
                            "4.Get Admin role");

            String inputCommandHead = scanner.nextLine();
            if (inputCommandHead.equals("1")) {
                boolean bigCategory = true;
                while (bigCategory) {
                    System.out.println("""
                            ---------------------------------------------------
                            | Please select one of them to use search options |
                            | Clothing                                        |
                            | Footwear                                        |
                            | Back                                            |
                            ---------------------------------------------------
                            """);
                    String inputCommandForBigCategory = scanner.nextLine();
                    if (inputCommandForBigCategory.equalsIgnoreCase("Clothing") || inputCommandForBigCategory.equalsIgnoreCase("Footwear")) {
                        boolean searchWhileByBoth = true;
                        while (searchWhileByBoth) {
                            System.out.println("""
                                    --------------------------------
                                    | Search ⩔                     |
                                    |        1.By category         |
                                    |        2.By gender           |
                                    |        3.By brand            |
                                    |        4.By price            |
                                    |        0.Back                |
                                    --------------------------------
                                    """);
                            String inputCommandForSearch = scanner.nextLine();

                            if (inputCommandForSearch.equals("1")) {
                                switch (inputCommandForBigCategory.toLowerCase()) {
                                    case "clothing" -> {
                                        System.out.println("We have such categories " + clothingController.getAllCategoryNames() + "\nEnter any of them:");
//                                        System.out.println(clothingController.getAllByCategory(scanner.nextLine()));
                                        displayResponsesInMarkdownForClothing((List<Clothing>) clothingController.getAllByCategory(scanner.nextLine()));
                                    }
                                    case "footwear" -> {
                                        System.out.println("We have such categories " + footwearController.getAllCategoryNames() + "\nEnter any of them:");
//                                        System.out.println(footwearController.getAllByCategory(scanner.nextLine()));
                                        displayResponsesInMarkdownForFootwear((List<Footwear>) footwearController.getAllByCategory(scanner.nextLine()));
                                    }
                                }
                            } else if (inputCommandForSearch.equals("2")) {
                                switch (inputCommandForBigCategory.toLowerCase()) {
                                    case "clothing" -> {
                                        System.out.println("We have such genders " + clothingController.getAllGenderNames() + "\nEnter any of them:");
//                                        System.out.println(clothingController.getAllByGender(scanner.nextLine()));
                                        displayResponsesInMarkdownForClothing((List<Clothing>) clothingController.getAllByGender(scanner.nextLine()));
                                    }
                                    case "footwear" -> {
                                        System.out.println("We have such genders " + footwearController.getAllGenderNames() + "\nEnter any of them:");
//                                        System.out.println(footwearService.getAllByGender(scanner.nextLine()));
                                        displayResponsesInMarkdownForFootwear((List<Footwear>) footwearService.getAllByGender(scanner.nextLine()));
                                    }
                                }
                            } else if (inputCommandForSearch.equals("3")) {
                                switch (inputCommandForBigCategory.toLowerCase()) {
                                    case "clothing" -> {
                                        System.out.println("We have such brands " + clothingController.getAllBrandNames() + "\nEnter any of them:");
//                                        System.out.println(clothingController.getAllByBrand(scanner.nextLine()));
                                        displayResponsesInMarkdownForClothing((List<Clothing>) clothingController.getAllByBrand(scanner.nextLine()));
                                    }
                                    case "footwear" -> {
                                        System.out.println("We have such brands " + footwearController.getAllBrandNames() + "\nEnter any of them:");
//                                        System.out.println(footwearController.getAllByBrand(scanner.nextLine()));
                                        displayResponsesInMarkdownForFootwear((List<Footwear>) footwearController.getAllByBrand(scanner.nextLine()));
                                    }

                                }
                            } else if (inputCommandForSearch.equals("4")) {
                                System.out.println("Please enter min price ");
                                String fromPrice = scanner.nextLine();
                                System.out.println("Please enter max price");
                                String toPrice = scanner.nextLine();
                                switch (inputCommandForBigCategory.toLowerCase()) {
//                                    case "clothing" -> System.out.println(clothingController.getAllByFromAndToPrice(fromPrice, toPrice));
                                    case "clothing" -> displayResponsesInMarkdownForClothing((List<Clothing>) clothingController.getAllByFromAndToPrice(fromPrice, toPrice));
//                                    case "footwear" -> System.out.println(footwearController.getAllByFromAndToPrice(fromPrice, toPrice));
                                    case "footwear" -> displayResponsesInMarkdownForFootwear((List<Footwear>) footwearController.getAllByFromAndToPrice(fromPrice, toPrice));
                                }
                                scanner = new Scanner(System.in);
                            } else if (inputCommandForSearch.equals("0")) {
                                searchWhileByBoth = false;
                            } else {
                                System.err.println("Invalid search command entered!");
                            }


                        }
                    } else if (inputCommandForBigCategory.equalsIgnoreCase("Back")) {
                        bigCategory = false;
                    } else {
                        System.err.println("Invalid type entered!");
                    }
                }

            }
            else if (inputCommandHead.equals("2")) {
                boolean bigCategory = true;
                while (bigCategory) {
                    System.out.println("""
                            Please select one of them to get all products by sort options 
                            Clothing
                            Footwear
                            Back
                            """);
                    String inputCommandForBigCategory = scanner.nextLine();
                    if (inputCommandForBigCategory.equalsIgnoreCase("Clothing") || inputCommandForBigCategory.equalsIgnoreCase("Footwear")) {
                        boolean sortWhile = true;
                        while (sortWhile) {
                            List<?> listsOfProducts = new ArrayList<>();
                            System.out.println("""
                                    --------------------------------
                                    | Sort ⩔                      |
                                    |        1.By price            |
                                    |        2.By brand(A-Z)       |
                                    |        0.Back                |
                                    --------------------------------
                                    """);
                            String inputCommandForSort = scanner.nextLine();
                            if (inputCommandForSort.equals("1")) {
                                System.out.println("1.From min to max\n2.From max to min");
                                String inputCommandForSortAndForPrice = scanner.nextLine();
                                if (inputCommandForSortAndForPrice.equals("1")) {
                                    switch (inputCommandForBigCategory.toLowerCase()) {
//                                        case "clothing" -> System.out.println(clothingController.getAllAsSortedFromMinToMax());
                                        case "clothing" -> displayResponsesInMarkdownForClothing((List<Clothing>) clothingController.getAllAsSortedFromMinToMax());
//                                        case "footwear" -> System.out.println(footwearController.getAllAsSortedFromMinToMax());
                                        case "footwear" -> displayResponsesInMarkdownForFootwear((List<Footwear>) footwearController.getAllAsSortedFromMinToMax());
                                    }
                                } else if (inputCommandForSortAndForPrice.equals("2")) {
                                    switch (inputCommandForBigCategory.toLowerCase()) {
                                        case "clothing" -> displayResponsesInMarkdownForClothing((List<Clothing>) clothingController.getAllAsSortedFromMaxToMin());
                                        case "footwear" -> displayResponsesInMarkdownForFootwear((List<Footwear>) footwearController.getAllAsSortedFromMaxToMin());
                                    }
                                } else {
                                    System.err.println("Invalid input entered!");
                                }

                            } else if (inputCommandForSort.equals("2")) {
                                System.out.println("1.A-Z.\n2.Z-A");
                                String inputCommandForSortAndForBrand = scanner.nextLine();
                                if (inputCommandForSortAndForBrand.equals("1")) {
                                    switch (inputCommandForBigCategory.toLowerCase()) {
                                        case "clothing" -> displayResponsesInMarkdownForClothing((List<Clothing>) clothingController.getAllAsSortedByBrandFromAToZ());
                                        case "footwear" -> displayResponsesInMarkdownForFootwear((List<Footwear>) footwearController.getAllAsSortedByBrandFromAToZ());
                                    }
                                } else if (inputCommandForSortAndForBrand.equals("2")) {
                                    switch (inputCommandForBigCategory.toLowerCase()) {
                                        case "clothing" -> displayResponsesInMarkdownForClothing((List<Clothing>) clothingController.getAllAsSortedByBrandFromZToA());
                                        case "footwear" -> displayResponsesInMarkdownForFootwear((List<Footwear>) footwearController.getAllAsSortedByBrandFromZToA());
                                    }
                                } else {
                                    System.err.println("INvalid input entered!");
                                }


                            } else if (inputCommandForSort.equals("0")) {
                                sortWhile = false;
                            }
                        }

                    } else if (inputCommandForBigCategory.equalsIgnoreCase("Back")) {
                        bigCategory = false;
                    } else {
                        System.err.println("Invalid input entered!");
                    }

                }


            }
            else if (inputCommandHead.equals("3")) {
                System.out.println("Stop command entered.\nThank you for your attention!");
                bigWhile = false;
            } else if (inputCommandHead.equals("4") && isAdmin==true){

            }else if (inputCommandHead.equals("5") && isAdmin==true){

            }else if (inputCommandHead.equals("6") && isAdmin==true){

            }else if (inputCommandHead.equals("4")  && isAdmin==false){
                System.out.println("Now you got admin role");
                continue;

            }


            else {
                System.err.println("Invalid command entered!");
            }
        }
    }

    public static void displayResponsesInMarkdownForClothing(List<Clothing> clothings) {
        if (clothings.size() == 0) {
            System.out.println("[]");
            return;
        }
        Table.Builder tableBuilder = new Table.Builder()
                .withAlignments(Table.ALIGN_LEFT)
                .withRowLimit(clothings.size() + 1)
                .addRow("id", "category", "name", "brand", "gender", "material", "quantity", "color", "size", "price", "detailSize");
        for (Clothing clothing : clothings) {
            tableBuilder.addRow(
                    clothing.getId(),
                    clothing.getCategory(),
                    clothing.getName(),
                    clothing.getBrand(),
                    clothing.getGender(),
                    clothing.getMaterial(),
                    clothing.getQuantity(),
                    clothing.getColor(),
                    clothing.getSize(),
                    clothing.getPrice(),
                    clothing.getDetailSize()
            );

        }
        System.out.println(tableBuilder.build());
    }

    public static void displayResponsesInMarkdownForFootwear(List<Footwear> footwears) {
        if (footwears.size() == 0) {
            System.out.println("[]");
            return;
        }
        Table.Builder tableBuilder = new Table.Builder()
                .withAlignments(Table.ALIGN_LEFT)
                .withRowLimit(footwears.size() + 1)
                .addRow("id", "category", "name", "brand", "gender", "material", "quantity", "color", "size", "price", "heel");
        for (Footwear footwear : footwears) {
            tableBuilder.addRow(
                    footwear.getId(),
                    footwear.getCategory(),
                    footwear.getName(),
                    footwear.getBrand(),
                    footwear.getGender(),
                    footwear.getMaterial(),
                    footwear.getQuantity(),
                    footwear.getColor(),
                    footwear.getSize(),
                    footwear.getPrice(),
                    footwear.getHeel()
            );

        }
        System.out.println(tableBuilder.build());
    }

    public static void displayReportFile() {
        Table.Builder table = new Table.Builder()
                .withAlignments(Table.ALIGN_RIGHT, Table.ALIGN_LEFT, Table.ALIGN_CENTER, Table.ALIGN_CENTER, Table.ALIGN_LEFT)
                .withRowLimit(10)
                .addRow("NUmber", "Stages", "Start date", "End Date", "Commit")
                .addRow(1, "Task Clarification", "06.04.2023", "08.04.2023", "Reviewed course project introduction ") //2 days
                .addRow(2, "Analysis", "09.04.2023", "12.04.2023", "Familiarized with the requirements")//3 days
                .addRow(3, "Use Cases", "13.04.2023", "17.04.2023", "What the commands are and how they work are discussed") //4 days
                .addRow(4, "Search for Solutions", "18.04.2023", "25.04.2023", "Researched , how to implement controller, service, dao, entity, exception layers")//1 week
                .addRow(5, "Software Development", "26.04.2023", "under development", "Almost finished project.Now Im optimizing code") //6 weeks
                .addRow(6, "Development Completion", "not started", "not started", "1 week")
                .addRow(7, "Presentation", "not started", "not started", "1 week");
        System.out.println(table.build());
    }


    public static void adminCommands() {

    }

    public static void userCommands() {

    }

}
