package edu.itpu.view;

import edu.itpu.entity.Clothing;
import edu.itpu.entity.Footwear;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class HelperView {
//   HelperView public static void main(String[] args) {
//        System.out.println(BLUE_BOLD_BRIGHT + """
//                    -----------------------------------------------------------------
//                    | Exist commands:                                               |
//                    | 1.Search                                                      |
//                    | 2.View all products                                           |
//                    | 3.Exit                                                        |
//                    -----------------------------------------------------------------
//                    """ + WHITE_BOLD_BRIGHT);
//        String inputCommandHead = scanner.nextLine();
//        if (inputCommandHead.equals("1")) {
//            boolean bigCategory = true;
//            while (bigCategory) {
//                System.out.println("""
//                            ---------------------------------------------------
//                            | Please select one of them to use search options |
//                            | Clothing                                        |
//                            | Footwear                                        |
//                            | Back                                            |
//                            ---------------------------------------------------
//                            """);
//                String inputCommandForBigCategory = scanner.nextLine();
//                if (inputCommandForBigCategory.equalsIgnoreCase("Clothing") || inputCommandForBigCategory.equalsIgnoreCase("Footwear")) {
//                    boolean searchWhileByBoth = true;
//                    while (searchWhileByBoth) {
//                        System.out.println("""
//                                    --------------------------------
//                                    | Search ⩔                     |
//                                    |        1.By category         |
//                                    |        2.By gender           |
//                                    |        3.By brand            |
//                                    |        4.By price            |
//                                    |        0.Back                |
//                                    --------------------------------
//                                    """);
//                        String inputCommandForSearch = scanner.nextLine();
//
//                        if (inputCommandForSearch.equals("1")) {
//                            switch (inputCommandForBigCategory.toLowerCase()) {
//                                case "clothing" -> {
//                                    System.out.println("We have such categories " + clothingController.getAllCategoryNames() + "\nEnter any of them:");
////                                        System.out.println(clothingController.getAllByCategory(scanner.nextLine()));
//                                    displayResponsesInMarkdownForClothing((List<Clothing>) clothingController.getAllByCategory(scanner.nextLine()));
//                                }
//                                case "footwear" -> {
//                                    System.out.println("We have such categories " + footwearController.getAllCategoryNames() + "\nEnter any of them:");
////                                        System.out.println(footwearController.getAllByCategory(scanner.nextLine()));
//                                    displayResponsesInMarkdownForFootwear((List<Footwear>) footwearController.getAllByCategory(scanner.nextLine()));
//                                }
//                            }
//                        } else if (inputCommandForSearch.equals("2")) {
//                            switch (inputCommandForBigCategory.toLowerCase()) {
//                                case "clothing" -> {
//                                    System.out.println("We have such genders " + clothingController.getAllGenderNames() + "\nEnter any of them:");
////                                        System.out.println(clothingController.getAllByGender(scanner.nextLine()));
//                                    displayResponsesInMarkdownForClothing((List<Clothing>) clothingController.getAllByGender(scanner.nextLine()));
//                                }
//                                case "footwear" -> {
//                                    System.out.println("We have such genders " + footwearController.getAllGenderNames() + "\nEnter any of them:");
////                                        System.out.println(footwearService.getAllByGender(scanner.nextLine()));
//                                    displayResponsesInMarkdownForFootwear((List<Footwear>) footwearService.getAllByGender(scanner.nextLine()));
//                                }
//                            }
//                        } else if (inputCommandForSearch.equals("3")) {
//                            switch (inputCommandForBigCategory.toLowerCase()) {
//                                case "clothing" -> {
//                                    System.out.println("We have such brands " + clothingController.getAllBrandNames() + "\nEnter any of them:");
////                                        System.out.println(clothingController.getAllByBrand(scanner.nextLine()));
//                                    displayResponsesInMarkdownForClothing((List<Clothing>) clothingController.getAllByBrand(scanner.nextLine()));
//                                }
//                                case "footwear" -> {
//                                    System.out.println("We have such brands " + footwearController.getAllBrandNames() + "\nEnter any of them:");
////                                        System.out.println(footwearController.getAllByBrand(scanner.nextLine()));
//                                    displayResponsesInMarkdownForFootwear((List<Footwear>) footwearController.getAllByBrand(scanner.nextLine()));
//                                }
//
//                            }
//                        } else if (inputCommandForSearch.equals("4")) {
//                            System.out.println("Please enter min price ");
//                            String fromPrice = scanner.nextLine();
//                            System.out.println("Please enter max price");
//                            String toPrice = scanner.nextLine();
//                            switch (inputCommandForBigCategory.toLowerCase()) {
////                                    case "clothing" -> System.out.println(clothingController.getAllByFromAndToPrice(fromPrice, toPrice));
//                                case "clothing" -> displayResponsesInMarkdownForClothing((List<Clothing>) clothingController.getAllByFromAndToPrice(fromPrice, toPrice));
////                                    case "footwear" -> System.out.println(footwearController.getAllByFromAndToPrice(fromPrice, toPrice));
//                                case "footwear" -> displayResponsesInMarkdownForFootwear((List<Footwear>) footwearController.getAllByFromAndToPrice(fromPrice, toPrice));
//                            }
//                            scanner = new Scanner(System.in);
//                        } else if (inputCommandForSearch.equals("0")) {
//                            searchWhileByBoth = false;
//                        } else {
//                            System.err.println("Invalid search command entered!");
//                        }
//
//
//                    }
//                } else if (inputCommandForBigCategory.equalsIgnoreCase("Back")) {
//                    bigCategory = false;
//                } else {
//                    System.err.println("Invalid type entered!");
//                }
//            }
//
//        } else if (inputCommandHead.equals("2")) {
//            boolean bigCategory = true;
//            while (bigCategory) {
//                System.out.println("""
//                            Please select one of them to get all products by sort options
//                            Clothing
//                            Footwear
//                            Back
//                            """);
//                String inputCommandForBigCategory = scanner.nextLine();
//                if (inputCommandForBigCategory.equalsIgnoreCase("Clothing") || inputCommandForBigCategory.equalsIgnoreCase("Footwear")) {
//                    boolean sortWhile = true;
//                    while (sortWhile) {
//                        List<?> listsOfProducts = new ArrayList<>();
//                        System.out.println("""
//                                    --------------------------------
//                                    | Sort ⩔                      |
//                                    |        1.By price            |
//                                    |        2.By brand(A-Z)       |
//                                    |        0.Back                |
//                                    --------------------------------
//                                    """);
//                        String inputCommandForSort = scanner.nextLine();
//                        if (inputCommandForSort.equals("1")) {
//                            System.out.println("1.From min to max\n2.From max to min");
//                            String inputCommandForSortAndForPrice = scanner.nextLine();
//                            if (inputCommandForSortAndForPrice.equals("1")) {
//                                switch (inputCommandForBigCategory.toLowerCase()) {
////                                        case "clothing" -> System.out.println(clothingController.getAllAsSortedFromMinToMax());
//                                    case "clothing" -> displayResponsesInMarkdownForClothing((List<Clothing>) clothingController.getAllAsSortedFromMinToMax());
////                                        case "footwear" -> System.out.println(footwearController.getAllAsSortedFromMinToMax());
//                                    case "footwear" -> displayResponsesInMarkdownForFootwear((List<Footwear>) footwearController.getAllAsSortedFromMinToMax());
//                                }
//                            } else if (inputCommandForSortAndForPrice.equals("2")) {
//                                switch (inputCommandForBigCategory.toLowerCase()) {
//                                    case "clothing" -> displayResponsesInMarkdownForClothing((List<Clothing>) clothingController.getAllAsSortedFromMaxToMin());
//                                    case "footwear" -> displayResponsesInMarkdownForFootwear((List<Footwear>) footwearController.getAllAsSortedFromMaxToMin());
//                                }
//                            } else {
//                                System.err.println("Invalid input entered!");
//                            }
//
//                        } else if (inputCommandForSort.equals("2")) {
//                            System.out.println("1.A-Z.\n2.Z-A");
//                            String inputCommandForSortAndForBrand = scanner.nextLine();
//                            if (inputCommandForSortAndForBrand.equals("1")) {
//                                switch (inputCommandForBigCategory.toLowerCase()) {
//                                    case "clothing" -> displayResponsesInMarkdownForClothing((List<Clothing>) clothingController.getAllAsSortedByBrandFromAToZ());
//                                    case "footwear" -> displayResponsesInMarkdownForFootwear((List<Footwear>) footwearController.getAllAsSortedByBrandFromAToZ());
//                                }
//                            } else if (inputCommandForSortAndForBrand.equals("2")) {
//                                switch (inputCommandForBigCategory.toLowerCase()) {
//                                    case "clothing" -> displayResponsesInMarkdownForClothing((List<Clothing>) clothingController.getAllAsSortedByBrandFromZToA());
//                                    case "footwear" -> displayResponsesInMarkdownForFootwear((List<Footwear>) footwearController.getAllAsSortedByBrandFromZToA());
//                                }
//                            } else {
//                                System.err.println("INvalid input entered!");
//                            }
//
//
//                        } else if (inputCommandForSort.equals("0")) {
//                            sortWhile = false;
//                        }
//                    }
//
//                } else if (inputCommandForBigCategory.equalsIgnoreCase("Back")) {
//                    bigCategory = false;
//                } else {
//                    System.err.println("Invalid input entered!");
//                }
//
//            }
//
//
//        } else if (inputCommandHead.equals("3")) {
//            System.out.println("Stop command entered.\nThank you for your attention!");
//            bigWhile = false;
//        } else {
//            System.err.println("Invalid command entered!");
//        }
//    }
}
