package edu.itpu.controller;

import edu.itpu.entity.Footwear;
import edu.itpu.service.FootwearService;
import edu.itpu.service.FootwearServiceImpl;

import java.util.List;
import java.util.Set;

public class FootwearControllerImpl implements FootwearController {
    private FootwearService footwearService;

    public FootwearControllerImpl(FootwearService footwearService) {
        this.footwearService = footwearService;
    }

    @Override
    public Set<?> getAllCategoryNames() {
        return footwearService.getAllCategoryNames();
    }

    @Override
    public List<?> getAllByCategory(String categoryName) {
        return footwearService.getALLByCategory(categoryName);
    }

    @Override
    public Set<?> getAllGenderNames() {
        return footwearService.getAllGenderNames();
    }

    @Override
    public List<?> getAllByGender(String genderName) {
        return footwearService.getAllByGender(genderName);
    }

    @Override
    public Set<?> getAllBrandNames() {
        return footwearService.getAllBrandNames();
    }

    @Override
    public List<?> getAllByBrand(String brandName) {
        return footwearService.getAllByBrand(brandName);
    }

    @Override
    public List<?> getAllByFromAndToPrice(String fromPrice, String toPrice) {
        return footwearService.getAllByFromAndToPrice(fromPrice, toPrice);
    }

    @Override
    public List<?> getAllAsSortedFromMinToMax() {
        return footwearService.getAllAsSortedFromMinToMax();
    }

    @Override
    public List<?> getAllAsSortedFromMaxToMin() {
        return footwearService.getAllAsSortedFromMaxToMin();
    }

    @Override
    public List<?> getAllAsSortedByBrandFromAToZ() {
        return footwearService.getAllAsSortedByBrandFromAToZ();
    }

    @Override
    public List<?> getAllAsSortedByBrandFromZToA() {
        return footwearService.getAllAsSortedByBrandFromZToA();
    }

    @Override
    public Footwear add(Footwear footwear) {
        return footwearService.add(footwear);
    }

    @Override
    public Set<String> addCategory(String category) {
        return footwearService.addCategory(category);
    }

    @Override
    public String delete(Integer id) {
        return footwearService.delete(id);
    }
}
