package edu.itpu.controller;

import edu.itpu.entity.Clothing;
import edu.itpu.entity.Footwear;

import java.util.List;
import java.util.Set;

public interface FootwearController {
    Set<?> getAllCategoryNames();

    List<?> getAllByCategory(String categoryName);

    Set<?> getAllGenderNames();

    List<?> getAllByGender(String genderName);

    Set<?> getAllBrandNames();

    List<?> getAllByBrand(String brandName);

    List<?> getAllByFromAndToPrice(String fromPrice, String toPrice);

    List<?> getAllAsSortedFromMinToMax();

    List<?> getAllAsSortedFromMaxToMin();

    List<?> getAllAsSortedByBrandFromAToZ();

    List<?> getAllAsSortedByBrandFromZToA();

    Footwear add(Footwear footwear);

    Set<String> addCategory(String category);

    String delete(Integer id);
}
