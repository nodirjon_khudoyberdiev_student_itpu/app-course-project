package edu.itpu.controller;

import edu.itpu.entity.User;

public interface UserController {

    String login(User user);

}
