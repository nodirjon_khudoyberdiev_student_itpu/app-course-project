package edu.itpu.controller;

import edu.itpu.entity.Clothing;
import edu.itpu.service.ClothingService;

import java.util.List;
import java.util.Set;

public class ClothingControllerImpl implements ClothingController {
    private final ClothingService clothingService;

    public ClothingControllerImpl(ClothingService clothingService) {
        this.clothingService = clothingService;
    }

    @Override
    public Set<?> getAllCategoryNames() {
        return clothingService.getAllCategoryNames();
    }

    @Override
    public List<?> getAllByCategory(String categoryName) {
        return clothingService.getAllByCategory(categoryName);
    }

    @Override
    public Set<?> getAllGenderNames() {
        return clothingService.getAllGenderNames();
    }

    @Override
    public List<?> getAllByGender(String genderName) {
        return clothingService.getAllByGender(genderName);
    }

    @Override
    public Set<?> getAllBrandNames() {
        return clothingService.getAllBrandNames();
    }

    @Override
    public List<?> getAllByBrand(String brandName) {
        return clothingService.getAllByBrand(brandName);
    }

    @Override
    public List<?> getAllByFromAndToPrice(String fromPrice, String toPrice) {
        return clothingService.getALlByFromAndToPrice(fromPrice, toPrice);
    }

    @Override
    public List<?> getAllAsSortedFromMinToMax() {
        return clothingService.getAllAsSortedFromMinToMax();
    }

    @Override
    public List<?> getAllAsSortedFromMaxToMin() {
        return clothingService.getAllAsSortedFromMaxToMin();
    }

    @Override
    public List<?> getAllAsSortedByBrandFromAToZ() {
        return clothingService.getAllAsSortedByBrandFromAToZ();
    }

    @Override
    public List<?> getAllAsSortedByBrandFromZToA() {
        return clothingService.getAllAsSortedByBrandFromZToA();
    }

    @Override
    public Clothing add(Clothing clothing) {
        return clothingService.add(clothing);
    }

    @Override
    public Set<String> addCategory(String category) {
        return clothingService.addCategory(category);
    }

    @Override
    public String delete(Integer id) {
        return clothingService.delete(id);
    }


}
