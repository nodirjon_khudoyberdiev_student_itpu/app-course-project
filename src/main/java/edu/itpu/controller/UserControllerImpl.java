package edu.itpu.controller;

import edu.itpu.entity.User;
import edu.itpu.service.UserService;

public class UserControllerImpl implements UserController{
    private UserService userService;

    public UserControllerImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public String login(User user) {
        return userService.login(user);
    }
}
