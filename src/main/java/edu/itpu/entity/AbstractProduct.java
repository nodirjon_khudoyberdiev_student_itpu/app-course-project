package edu.itpu.entity;

import java.util.Objects;

public abstract class AbstractProduct {
    private Integer id;
    private String category;
    private String name;
    private String brand;
    private String gender;
    private String material;
    private int quantity;
    private String color;
    private int size;
    private Double price;

    public AbstractProduct() {
    }

    public AbstractProduct(Integer id, String category, String name, String brand, String gender, String material, int quantity, String color, int size, Double price) {
        this.id = id;
        this.category = category;
        this.name = name;
        this.brand = brand;
        this.gender = gender;
        this.material = material;
        this.quantity = quantity;
        this.color = color;
        this.size = size;
        this.price = price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    protected String generalFields() {
        return "id=" + id +
                ", category='" + category + '\'' +
                ", name='" + name + '\'' +
                ", brand='" + brand + '\'' +
                ", gender='" + gender + '\'' +
                ", material='" + material + '\'' +
                ", quantity=" + quantity +
                ", color='" + color + '\'' +
                ", size=" + size +
                ", price=" + price;
    }

    @Override
    public String toString() {
        return "AbstractProduct{" +
                generalFields() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractProduct that = (AbstractProduct) o;
        return quantity == that.quantity && size == that.size && id.equals(that.id) && category.equals(that.category) && name.equals(that.name) && brand.equals(that.brand) && gender.equals(that.gender) && material.equals(that.material) && color.equals(that.color) && price.equals(that.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, category, name, brand, gender, material, quantity, color, size, price);
    }
}
