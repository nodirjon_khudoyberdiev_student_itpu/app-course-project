package edu.itpu.entity;

import java.util.Objects;

public class Clothing extends AbstractProduct {
    private String detailSize;

    public Clothing() {
    }

    public Clothing(Integer id, String category, String name, String brand, String gender, String material, int quantity, String color, int size, Double price, String detailSize) {
        super(id, category, name, brand, gender, material, quantity, color, size, price);
        this.detailSize = detailSize;
    }

    public String getDetailSize() {
        return detailSize;
    }

    public void setDetailSize(String detailSize) {
        this.detailSize = detailSize;
    }


    @Override
    public String toString() {
        return "\nClothing{" +
                generalFields() +
                "detailSize='" + detailSize + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Clothing clothing = (Clothing) o;
        return detailSize.equals(clothing.detailSize);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), detailSize);
    }
}
