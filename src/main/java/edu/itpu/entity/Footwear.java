package edu.itpu.entity;

import java.util.Objects;

public class Footwear extends AbstractProduct {
    private String heel;

    public Footwear() {
    }

    public Footwear(Integer id, String category, String name, String brand, String gender, String material, int quantity, String color, int size, Double price, String heel) {
        super(id, category, name, brand, gender, material, quantity, color, size, price);
        this.heel = heel;
    }

    public String getHeel() {
        return heel;
    }

    public void setHeel(String heel) {
        this.heel = heel;
    }

    @Override
    public String toString() {
        return "\nFootwear{" +
                generalFields() +
                "heel='" + heel + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Footwear footwear = (Footwear) o;
        return heel.equals(footwear.heel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), heel);
    }
}
