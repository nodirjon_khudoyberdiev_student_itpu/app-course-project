package edu.itpu.dao;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FootwearDAOImplTest {

    @Mock
    private FootwearDAO footwearDAO;


    @BeforeEach
    void setUp() {
        footwearDAO = Mockito.mock(FootwearDAO.class);
    }

    @Test
    void getDataFromCsvFile() {
        List<String[]> expected = new ArrayList<>();
        expected.add(new String[]{"6","Sneakers","Vomero","GUCCI","men","leather","50","blue","43","250","medium"});
        expected.add(new String[]{"7","Sneakers","Mac","Prada","men","fabric","50","redline","44","450","no heel"});
        expected.add(new String[]{"8","Pumps","Spike","Coperni","women","fabric","50","white","45","890","high"});
        Mockito.when(footwearDAO.getDataFromCsvFile()).thenReturn(expected);
        List<String[]> actual = footwearDAO.getDataFromCsvFile();
        assertEquals(expected,actual);
        assertEquals(3,actual.size());
    }

}