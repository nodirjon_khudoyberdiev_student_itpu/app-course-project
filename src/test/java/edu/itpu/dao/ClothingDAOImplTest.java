package edu.itpu.dao;

import jdk.jfr.Name;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ClothingDAOImplTest {

    @Mock
    private ClothingDAO clothingDAO;


    @BeforeEach
    void setUp() {
        clothingDAO=Mockito.mock(ClothingDAO.class);
    }

    @Test
    void getDataFromCsvFile() {
        List<String[]> expected = new ArrayList<>();
        expected.add(new String[]{"1","Jackets","Jacquard","GUCCI","men","cotton","45","canvas","41","3250","90","x60x90"});
        expected.add(new String[]{"2","Jackets","Houndstooth","GUCCI","men","cotton","55","brown","42","2740","90x50x90"});
        Mockito.when(clothingDAO.getDataFromCsvFile()).thenReturn(expected);
        List<String[]> actual = clothingDAO.getDataFromCsvFile();
        assertEquals(expected,actual);
        assertEquals(2,actual.size());
    }


}